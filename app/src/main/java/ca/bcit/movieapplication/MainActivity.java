package ca.bcit.movieapplication;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ActionBar bar = getSupportActionBar();
        if (bar != null) {
            getSupportActionBar().setTitle("Home");
        }
    }

    public void onClickGoToAddMovie(View view) {
        Intent intent = new Intent(this, AddMovieActivity.class);
        startActivity(intent);
    }
}
