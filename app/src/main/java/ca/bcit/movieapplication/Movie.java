package ca.bcit.movieapplication;

/**
 * A Movie item to store in the Movie Catalogue Database.
 */
public class Movie {
    private String title;
    private String description;
    private String onlineResource;

    /***
     * Initialize a Movie object.
     * @param title a String.
     * @param description a String description of the Movie.
     * @param onlineResource a URL for an online resource about the Movie.
     */
    public Movie(String title, String description, String onlineResource) {
        this.title = title;
        this.description = description;
        this.onlineResource = onlineResource;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getOnlineResource() {
        return onlineResource;
    }

    public void setOnlineResource(String onlineResource) {
        this.onlineResource = onlineResource;
    }
}
