package ca.bcit.movieapplication;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.firebase.firestore.FirebaseFirestore;

public class AddMovieActivity extends AppCompatActivity {
    private static final String LOG_TAG = "Add Movie";
    private FirebaseFirestore db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_movie);
        db = FirebaseFirestore.getInstance();

        ActionBar bar = getSupportActionBar();
        if (bar != null) {
            bar.setDisplayHomeAsUpEnabled(true);
        }
    }

    public void onClickSaveMovie(View view) {
        EditText titleView = (EditText) findViewById(R.id.title);
        String title = titleView.getText().toString();

        EditText descriptionView = (EditText) findViewById(R.id.description);
        String description = descriptionView.getText().toString();

        EditText onlineResourceView = (EditText) findViewById(R.id.link);
        String onlineResource = onlineResourceView.getText().toString();

        // Create a Movie object and save it to the database.
        Movie newMovie = new Movie(title, description, onlineResource);
        addMovieToDatabase(newMovie);

        // Clear the form fields.
        titleView.getText().clear();
        descriptionView.getText().clear();
        onlineResourceView.getText().clear();

    }

    /**
     * Add a Movie object to the database.
     * @param movie a Movie to add to the Catalogue.
     */
    private void addMovieToDatabase(Movie movie) {
        db.collection("movies")
                .add(movie)
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.d(LOG_TAG, "Failed to save Movie.", e);
                    }
                });
    }
}
